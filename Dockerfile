FROM python:3.6

WORKDIR /code

COPY requirements.txt /code

RUN pip install -r requirements.txt --no-cache-dir

CMD ["jupyter-notebook", "--no-browser", "--ip=0.0.0.0", "--allow-root"]
